import os
import sys
import traceback
from heapq import *
from collections import defaultdict


def calculate_frequencies(text):
        chars_with_frequency = defaultdict(int)
        for character in text:
                    chars_with_frequency[character] += 1

        return chars_with_frequency

class Node(object):
        left = None
        right = None
        item = None
        weight = 0

        def __init__(self, i, w):
                self.item = i
                self.weight = w

        def setChildren(self, ln, rn):
                self.left = ln
                self.right = rn

        def __repr__(self):
                return "{} -> {} _ {} _ {}".format(self.item, self.weight, self.left, self.right)

        def __cmp__(self, a):
                return cmp(self.weight, a.weight)


def build_tables(s, node, coding, decoding):
        if node.item:
                if not s:
                        coding[node.item] = "0"
                        decoding["0"] = node.item
                else:
                        coding[node.item] = s
                        decoding[s] = node.item
        else:
                build_tables(s+"0", node.left, coding, decoding)
                build_tables(s+"1", node.right, coding, decoding)


def huffman(input):
	itemqueue =  [Node(character, frequency) for character, frequency in calculate_frequencies(input).items()]
	heapify(itemqueue)
	while len(itemqueue) > 1:
		l = heappop(itemqueue)
		r = heappop(itemqueue)
                
		n = Node(None, r.weight+l.weight)
		n.setChildren(l,r)
		heappush(itemqueue, n)



        coding = {}
        decoding = {}
        build_tables("",itemqueue[0], coding, decoding)

	return decoding, "".join([coding[a] for a in input])


def get_huffman_header(decode_table):
        header = ""
        header += (str(len(decode_table)))
        header += ("\nhuffman_teoria_de_codigos\n")
        for code, character in decode_table.items():
                header += ("{}${}\n".format(code, character))

        return header

        

def encode_file(file_name):
        try:
                txt = file(file_name).read()
        except IOError:
                print "Comprueba que el fichero que indicas existe."
                return

        decode_table, compressed_bin = huffman(txt)
        compressed_bin_len = len(compressed_bin)

        with open(file_name + ".huffman", 'w') as compressed_file:
                # write compress_table
                binary_compressed = (get_huffman_header(decode_table))
                # map compressed_text to chars
                
                i = 0
                while i < compressed_bin_len-8:
                        binary_compressed += chr(int(compressed_bin[i:i+8], 2))
                        i+=8

                last_length = len(compressed_bin[i:])
                binary_compressed += chr(int(compressed_bin[i:], 2)) + str(last_length)
                
                compressed_file.write(binary_compressed)

        compressed_size = len(binary_compressed)/1024.0
        original_size = os.stat(file_name).st_size/1024.0

        print 'Tamanho original: {} KB'.format(int(original_size))
        print 'Tamanho comprimido: {} KB'.format(int(compressed_size))
        print 'Ratio de compresion de: {}'.format(compressed_size/original_size)

def read_huffman_header(compressed_file):
        num_entries = int(compressed_file.readline())

        protocol = compressed_file.readline()
        if protocol.strip() != "huffman_teoria_de_codigos":
                raise ValueError

        decode_table = {}
        for i in range(num_entries):
                line = compressed_file.readline()
                if '$' not in line:
                        raise ValueError
                
                values = line.split('$')
                # solucion para cuando se encuentre con un salto de linea. Si no fallaria y encontraria
                # una linea vacia.
                if len(values[1]) == 1:
                        decode_table[values[0]] = '\n'
                        compressed_file.readline()
                else:
                        decode_table[values[0]] = values[1][:-1]
                        
        return decode_table
        
def bin_str_to_org_str(bin_str, decode_table):
        begin_index = 0
        current_index = 0
        rebuilt_str = ''
        while current_index < len(bin_str) + 1:
                try:
                        rebuilt_str += decode_table[str(bin_str[begin_index:current_index])]
                        begin_index = current_index
                        
                except KeyError:
                        current_index += 1
                        
        return rebuilt_str

def decode_file(file_name):
        try:
                bin_str = ""
                with open(file_name, 'r') as compressed_file:

                        decode_table = read_huffman_header(compressed_file)
                        content = compressed_file.read()
                        bin_str = ''
                        last_two_chars = content[len(content) - 2:] 
                        normal = content[:len(content) - 2]
                        for character in normal:
                                temp = bin(ord(character))[2:] # strip '0b'
                                extra_space = 8 - len(temp) 
                                zeros = '0'*extra_space
                                temp = zeros + temp
                                bin_str += temp
                                
                        last_char_bin = bin(ord(last_two_chars[0]))[2:] 
                        last_char_bin = '0'*(int(last_two_chars[1]) - len(last_char_bin)) + last_char_bin
                                                 
                        bin_str += last_char_bin

                with open(file_name+".decompressed", 'w') as decompressed_file:
                        decompressed = bin_str_to_org_str(bin_str, decode_table)
                        decompressed_file.write(decompressed)
                        
        except IOError:
                print "Comprueba que el fichero que indicas existe."
                return
        except ValueError:
                traceback.print_exc()
                print "Fichero incompatible"
                return
        
if __name__ == "__main__":
        if sys.argv[1] == 'comprimir':
                encode_file(sys.argv[2])
        elif sys.argv[1] == 'descomprimir':
                decode_file(sys.argv[2])
        else:
                print "Error de sintaxis. Prueba con huffman.py [comprimir] [nombre_fichero]"
